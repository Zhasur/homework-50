class Machine {
    constructor() {
        this._turnOn = false;
    }

    turnOn() {
        this._turnOn = true;
        console.log('Machine is turned on!');
    };

    turnOff() {
        this._turnOn = false;
        console.log('Machine is turned off!');
    }

}

class HomeAppliance extends Machine {
    constructor() {
        super();
        this._plugIn = false;
    }

    plugIn() {
        this._plugIn = true;
        console.log('Machine is plugged in!');
    };

    plugOff() {
        this._plugIn = false;
        console.log('Machine is plugged off!');
    }
}

class WashingMachine extends HomeAppliance {
    constructor() {
        super();
    }

    turnOn() {
        if (this._plugIn) {
            this._turnOn = true;
            console.log('Machine is turned on!');
        } else {
            console.log('Please check the power!')
        }
    };

    run() {
        if (this._plugIn && this._turnOn) {
            console.log('Machine is started to wash');
        }
    };
}

class LightSource extends HomeAppliance {
    constructor() {
        super();
        this.level = 0;
    }

    setLevel(level) {
        if (level >= 0 && level <= 100) {
            this.level = level;
            console.log('Level set is ' + this.level);
        } else {
            console.log('Number is not correct')
        }
    }

}

class AutoVehicle extends Machine {
   constructor(){
       super();
       this._x = 0;
       this._y = 0;
   }

    setPosition(x, y) {
        this._x = x;
        this._y = y;
        console.log('Car is in coordination ' + this._x + ' ' + this._y);
    }
}

class Car extends AutoVehicle {
    constructor() {
        super();
        this._speed = 10;

        this._cordX = 0;
        this._cordY = 0;
    }

    setSpeed(speed) {
        this._speed = speed;
    };

    run(x, y) {
        const interval = setInterval(() => {
            let newX = this._x + this._speed;
            let newY = this._y + this._speed;
            if (newX > x) newX = x;
            if (newY > y) newY = y;
            this.setPosition(newX, newY);
            if (newX === x && newY === y) {
                clearInterval(interval);
                console.log('Car is on its destination!');
            }
        }, 1000);
    }
}